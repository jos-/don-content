#!/usr/bin/env python3
import argparse
from collections import defaultdict
from pathlib import Path

import yaml


def main(directory: Path) -> int:
    api_directory = directory / "api"
    api_ids = {file.stem for file in api_directory.glob("*.yaml")}

    repository_directory = directory / "repository"

    has_failures = False

    for file in sorted(repository_directory.glob("*.yaml")):
        with file.open() as f:
            instance = yaml.safe_load(f)

        errors: dict[str, list[str]] = defaultdict(list)

        for repository in instance["repositories"]:
            if "apis" not in repository:
                continue

            for api in repository["apis"]:
                if api not in api_ids:
                    errors[repository_name(repository)].append(api)

        if errors:
            has_failures = True
            print(f"{file}:")
            for repository, apis in errors.items():
                print(f"  Undefined API for repository: '{repository}': {apis}")

    return 1 if has_failures else 0


def repository_name(repository: dict[str, str]) -> str:
    if any((key := k) in repository for k in ("github", "gitlab")):
        return f"{key}: {repository[key]}"

    return ""


def parse_aruments() -> argparse.Namespace:
    parser = argparse.ArgumentParser(description="Validate repository files")

    parser.add_argument("directory", help="Content directory to validate")

    return parser.parse_args()


if __name__ == "__main__":
    args = parse_aruments()
    exit(main(Path(args.directory)))
